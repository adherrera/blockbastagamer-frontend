import { Component, OnInit } from '@angular/core';
import { Juego } from './juego';
import { JuegoService } from './juego.service';
import { AlertService } from '../alert/alert.service';
import { Router, ActivatedRoute } from '@angular/router';
import { CompanyService } from '../companies/company.service';
import { Company } from '../companies/company';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
})
export class FormComponent implements OnInit {

  title: string;

  juego: Juego = new Juego();

  categorias: string[] = ['ACTION', 'SHOOTER', 'STRATEGY', 'SIMULATION', 'SPORT',
                          'RACE', 'ADVENTURE', 'ROL', 'SANDBOX', 'MUSICAL', 'PUZZLE', 'EDUCATION'];

  companies: Company[];

  constructor(
    private juegoService: JuegoService,
    private alertService: AlertService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private companyService: CompanyService
  ) { }

  ngOnInit(): void {
    this.loadJuego();
    this.loadCompanies();
  }

  create(): void {
    this.juegoService.createJuego(this.juego).subscribe(
      juego => {
        console.log(juego);
        this.alertService.success(`El juego ${juego.titulo} se ha creado con éxito con Id:${juego.id}`, {autoClose: true});
        this.router.navigate(['/juegos']);
      }
    );
  }

  loadJuego(): void {
    this.activatedRoute.params.subscribe(
      params => {
        const id = params.id;
        if (id) {
          this.title = 'Editar juego';
          this.juegoService.getJuego(id).subscribe(
            juego => this.juego = juego
          );
        } else {
          this.title = 'Añadir juego';
        }
      }
    );
  }

  update(): void {
    this.juegoService.updateJuego(this.juego).subscribe(
      juego => {
        this.alertService.success(`El juego ${juego.titulo} con Id:${juego.id}se ha actualizado con éxito`, {autoClose: true});
        this.router.navigate(['/juegos']);
      }
    );
  }

  loadCompanies(): void {
    this.companyService.getCompanies().subscribe(
      companies => this.companies = companies
    );
  }

  compareCompany(companyToCompare: Company, companySelected: Company): boolean {
    if (!companyToCompare || !companySelected) {
      return false;
    }
    return companyToCompare.id === companySelected.id;
  }

}
