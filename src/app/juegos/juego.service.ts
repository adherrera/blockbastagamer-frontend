import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Juego } from './juego';
import { map, catchError } from 'rxjs/operators';
import { AlertService } from '../alert/alert.service';

@Injectable({
  providedIn: 'root'
})
export class JuegoService {

  private urlEndPoint = 'http://localhost:8090/api/juegos';

  private httpHeaders = new HttpHeaders({'Content-Type': 'application/json'});

  constructor(
    private http: HttpClient,
    private alertService: AlertService
    ) { }

  getJuegos(): Observable<Juego[]> {
    return this.http.get(this.urlEndPoint).pipe(
      map( response => response as Juego[] ),     // Cuando se usa el map, no hace falta declarar el tipo al lado del tipo de petición.
      catchError( error => {
        console.log('Se ha producido un error al obtener la lista de juegos');
        console.error(`getJuegos Error: "${error.message}`);
        this.alertService.error(`Error al consultar la lista de juegos: "${error.message}"`, {autoClose: true});
        return throwError(error);
      })
    );
  }

  getJuego(id: number): Observable<Juego> {
    return this.http.get<Juego>(`${this.urlEndPoint}/${id}`).pipe(    // Aquí si se añade porque no mapeamos lo que llega de la BBDD.
      catchError( error => {
        console.log('Se ha producido un error al obtener el juego');
        console.error(`getJuego Error: "${error.message}`);
        this.alertService.error(`Error al obtener el juego ${id}: "${error.message}"`, {autoClose: true});
        return throwError(error);
      })
    );
  }

  createJuego(juego: Juego): Observable<Juego> {
    return this.http.post<Juego>(this.urlEndPoint, juego, {headers: this.httpHeaders}).pipe(
      catchError( error => {
        console.log('Se ha producido un error al añadir el juego');
        console.error(`createJuego Error: "${error.message}`);
        if (error.status === 400) {
          error.error.errorMessage.replace('[', '').replace(']', '').split(', ').reverse().forEach(errorMessage => {
            this.alertService.error(`Uno de los campos: "${errorMessage}"`, {autoClose: true});
          });
        } else {
          this.alertService.error(`Error al crear el juego: "${error.message}"`, {autoClose: true});
        }
        return throwError(error);
      })
    );
  }

  updateJuego(juego: Juego): Observable<Juego> {
    return this.http.put<Juego>(`${this.urlEndPoint}/${juego.id}`, juego, {headers: this.httpHeaders}).pipe(
      catchError( error => {
        console.log('Se ha producido un error al actualizar juego');
        console.error(`updateJuego Error: "${error.message}`);
        if (error.status === 400) {
          error.error.errorMessage.replace('[', '').replace(']', '').split(', ').reverse().forEach(errorMessage => {
            this.alertService.error(`Uno de los campos ${errorMessage}`, {autoClose: true});
          });
        } else {
          this.alertService.error(`Error al actualizar el juego: "${error.message}"`, {autoClose: true});
        }
        return throwError(error);
      })
    );
  }

  deleteJuego(id: number): Observable<any> {
    return this.http.delete(`${this.urlEndPoint}/${id}`).pipe(
      catchError( error => {
        console.log('Se ha producido un error al eliminar el juego');
        console.error(`deleteJuego Error: "${error.message}`);
        this.alertService.error(`Error al eliminar el juego: "${error.message}"`, {autoClose: true});
        return throwError(error);
      })
    );
  }


}
