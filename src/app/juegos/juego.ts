import { Company } from '../companies/company';

export class Juego {
  id: number;
  titulo: string;
  fechaLanzamiento: string;
  categoria: string;
  pegi: number;
  listaCompanies: Company[];
}
