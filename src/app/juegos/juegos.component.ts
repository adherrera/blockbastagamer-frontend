import { Component, OnInit } from '@angular/core';
import { Juego } from './juego';
import { JuegoService } from './juego.service';
import { Router } from '@angular/router';
import { AlertService } from '../alert/alert.service';
import { Company } from '../companies/company';


@Component({
  selector: 'app-juegos',
  templateUrl: './juegos.component.html',
})
export class JuegosComponent implements OnInit {

  juegos: Juego[] = [];
  showId = false;

  constructor(
    private juegoService: JuegoService,
    private router: Router,
    private alertService: AlertService) { }

  ngOnInit(): void {
    this.refreshJuegos();
  }
  switchId(): void {
    this.showId = !this.showId;
  }

  ocultarMostrarId(): string {
    return this.showId ? 'Ocultar Id' : 'Mostrar Id';
  }

  delete(id: number): void {
    if (confirm(`¿Está seguro de que desea eliminar el juego con Id:${id}?`)) {
      this.juegoService.deleteJuego(id).subscribe(
        response => {
          this.alertService.success(`El juego con Id:${id} se ha eliminado con éxito`, {autoClose: true});
          this.refreshJuegos();
          this.router.navigate(['/juegos']);
        }
      );
    }
  }

  refreshJuegos(): void {
    this.juegoService.getJuegos().subscribe(
      juegos => this.juegos = juegos
    );
  }

  listCompanies(list: Company[]): string {
    return list.map(company => company.nombreCompany).toString().replace(',', ', ');
  }

}
