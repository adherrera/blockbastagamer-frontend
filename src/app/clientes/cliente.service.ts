import { Injectable } from '@angular/core';
import { Cliente } from './cliente';
import { Observable, throwError } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { AlertService } from '../alert/alert.service';


@Injectable({
  providedIn: 'root'
})
export class ClienteService {

  private urlEndPoint = 'http://localhost:8090/api/clientes';

  private httpHeaders = new HttpHeaders({'Content-Type': 'application/json'});

  constructor(
    private http: HttpClient,
    private alertService: AlertService) {}

  getClientes(): Observable<Cliente[]> {
    return this.http.get(this.urlEndPoint).pipe(
      map( response => response as Cliente[] ),
      catchError (e => {
        this.alertService.error(`Error al consultar los clientes:"${e.message}"`, {autoClose: true, keepAfterRouteChange: false} );
        return throwError(e);
      })
    );
  }


  create(cliente: Cliente): Observable<Cliente> {
    return this.http.post<Cliente>(this.urlEndPoint, cliente, {headers: this.httpHeaders}).pipe(
      catchError (e => {
        console.error(e.error.message);
        this.alertService.error(`Error al crear el cliente:"${e.message}"`, {autoClose: true, keepAfterRouteChange: false} );
        return throwError(e);
      })
    );
  }

  getCliente(nif: string): Observable<Cliente> {
    return this.http.get<Cliente>(`${this.urlEndPoint}/${nif}`).pipe(
      catchError (e => {
        console.error(e.error.message);
        this.alertService.error(`Error al obtener el cliente:"${e.message}"`, {autoClose: true, keepAfterRouteChange: false} );
        return throwError(e);
      })
    );
  }

  update(cliente: Cliente): Observable<Cliente> {
    return this.http.put<Cliente>(`${this.urlEndPoint}/${cliente.nif}`, cliente, {headers: this.httpHeaders}).pipe(
      catchError (e => {
        console.error(e.error.message);
        this.alertService.error(`Error al actualizar el cliente:"${e.message}"`, {autoClose: true, keepAfterRouteChange: false} );
        return throwError(e);
      })
    );
  }
}
