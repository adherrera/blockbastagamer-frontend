export class Cliente {
    id: number;
    nombreCliente: string;
    nif: string;
    fechaNacimiento: Date;
    correo: string;
    nombreUsuario: string;
    contrasenia: string;
    // listaRoles: Rol[];
    // listaStocks: Stock[];
}
