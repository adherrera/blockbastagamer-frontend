import { Component, OnInit } from '@angular/core';
import { Cliente } from './cliente';
import { ClienteService } from './cliente.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertService } from '../alert/alert.service';

@Component({
  selector: 'app-form',
  templateUrl: './form.component.html',
})
export class FormComponent implements OnInit {

  titulo = 'Formulario de registro de cliente';
  cliente: Cliente = new Cliente();

  constructor(
    private clienteService: ClienteService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private alertService: AlertService
    ) { }

  ngOnInit(): void {
    this.loadCliente();
  }

  public create(): void {
    this.clienteService.create(this.cliente).subscribe(
      response => this.router.navigate(['/clientes'])
    );
  }

  public update(): void {
    this.clienteService.update(this.cliente).subscribe(
      cliente => {
        this.router.navigate(['/clientes']);
        this.alertService.success(`Cliente ${cliente.nif} actualizado con éxito`);
      }
    );
  }

  loadCliente(): void {
    this.activatedRoute.params.subscribe(
      params => {
        const nif = params.nif;
        if (nif) {
          this.clienteService.getCliente(nif).subscribe(
            (cliente) => this.cliente = cliente
          );
        }
      }
    );
  }

}
