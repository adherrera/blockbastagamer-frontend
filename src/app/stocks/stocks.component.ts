import { Component, OnInit } from '@angular/core';
import { Stock } from './stock';
import { StockService } from './stock.service';

@Component({
  selector: 'app-stocks',
  templateUrl: './stocks.component.html',
})
export class StocksComponent implements OnInit {
  stocks: Stock[];
  showId = false;

  constructor(private stockService: StockService) { }

  ngOnInit(): void {
    this.stockService.getStocks().subscribe(
      stocks => this.stocks = stocks
    );
  }
  switchId(): void {
    this.showId = !this.showId;
  }
}
