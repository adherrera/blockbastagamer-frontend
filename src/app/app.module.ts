import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { ClientesComponent } from './clientes/clientes.component';
import { ClienteService } from './clientes/cliente.service';
import { Routes, RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { JuegosComponent } from './juegos/juegos.component';
import { JuegoService } from './juegos/juego.service';
import { CompaniesComponent } from './companies/companies.component';
import { CompanyService } from './companies/company.service';
import { RolesComponent } from './roles/roles.component';
import { RolService } from './roles/rol.service';
import { TiendasComponent } from './tiendas/tiendas.component';
import { TiendaService } from './tiendas/tienda.service';
import { StocksComponent } from './stocks/stocks.component';
import { StockService } from './stocks/stock.service';
import { FormComponent as ClientesFormComponent } from './clientes/form.component';
import { FormsModule } from '@angular/forms';
import { AlertComponent } from './alert/alert.component';
import { AlertService } from './alert/alert.service';
import { FormComponent as JuegosFormComponent } from './juegos/form.component';

const routes: Routes = [
  {path: '', redirectTo: '/juegos', pathMatch: 'full'},
  {path: 'roles', component: RolesComponent},
  {path: 'clientes', component: ClientesComponent},
  {path: 'clientes/form', component: ClientesFormComponent},
  {path: 'clientes/form/:nif', component: ClientesFormComponent},
  {path: 'tiendas', component: TiendasComponent},
  {path: 'companies', component: CompaniesComponent},
  {path: 'juegos', component: JuegosComponent},
  {path: 'juegos/form', component: JuegosFormComponent},
  {path: 'juegos/form/:id', component: JuegosFormComponent},
  {path: 'stocks', component: StocksComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    ClientesComponent,
    JuegosComponent,
    CompaniesComponent,
    RolesComponent,
    TiendasComponent,
    StocksComponent,
    ClientesFormComponent,
    AlertComponent,
    JuegosFormComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(routes)
  ],
  providers: [
    ClienteService,
    JuegoService,
    CompanyService,
    StockService,
    TiendaService,
    RolService,
    AlertService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
