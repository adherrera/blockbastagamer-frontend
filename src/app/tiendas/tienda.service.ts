import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Tienda } from './tienda';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TiendaService {

  urlEndPoint = 'http://localhost:8090/api/tiendas';

  constructor(private http: HttpClient) { }

  getTiendas(): Observable<Tienda[]> {
    return this.http.get(this.urlEndPoint).pipe(
      map( response => response as Tienda[])
    );
  }
}
