import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Company } from './company';
import { catchError } from 'rxjs/operators';
import { AlertService } from '../alert/alert.service';

@Injectable({
  providedIn: 'root'
})
export class CompanyService {

  private urlEndPoint = 'http://localhost:8090/api/companies';

  constructor(
    private http: HttpClient,
    private alertService: AlertService
    ) {}

  getCompanies(): Observable<Company[]> {
    return this.http.get<Company[]>(this.urlEndPoint).pipe(
      catchError( error => {
        console.error(`getCompanies Error: "${error.message}`);
        if (error.status === 401) {
        } else {
          this.alertService.error(`Error al consultar la lista de compañías: "${error.message}"`, {autoClose: true});
        }
        return throwError(error);
      })
    );
  }

}
