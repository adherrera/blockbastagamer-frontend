import { Juego } from '../juegos/juego';

export class Company {
    id: number;
    cif: string;
    nombreCompany: string;
    listaJuegos: Juego[];
}
