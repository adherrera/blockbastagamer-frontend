import { Component, OnInit } from '@angular/core';
import { Company } from './company';
import { CompanyService } from './company.service';
import { Juego } from '../juegos/juego';

@Component({
  selector: 'app-companies',
  templateUrl: './companies.component.html',
})
export class CompaniesComponent implements OnInit {

  companies: Company[] = [];
  showId: boolean;

  constructor(private companyService: CompanyService) { }

  ngOnInit(): void {
    this.refreshCompanies();
  }

  switchId(): void {
    this.showId = !this.showId;
  }

  ocultarMostrarId(): string {
    return this.showId ? 'Ocultar Id' : 'Mostrar Id';
  }

  refreshCompanies(): void {
    this.companyService.getCompanies().subscribe(
      companies => this.companies = companies
    );
  }

  listJuegos(list: Juego[]): string {
    return list.map(juego => juego.titulo).toString().replace(',', ', ');
  }
}
