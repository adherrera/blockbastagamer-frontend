import { Component, OnInit } from '@angular/core';
import { Rol } from './rol';
import { RolService } from './rol.service';

@Component({
  selector: 'app-roles',
  templateUrl: './roles.component.html',
})
export class RolesComponent implements OnInit {

  roles: Rol[];
  showId = false;

  constructor(private rolService: RolService) { }

  ngOnInit(): void {
    this.rolService.getRoles().subscribe(
      roles => this.roles = roles
    );
  }
  switchId(): void {
    this.showId = !this.showId;
  }

}
