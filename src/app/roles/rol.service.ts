import { Injectable } from '@angular/core';
import { Rol } from './rol';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Juego } from '../juegos/juego';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RolService {

  private urlEndPoint = 'http://localhost:8090/api/roles';

  constructor(private http: HttpClient) { }

  getRoles(): Observable<Rol[]> {
    return this.http.get(this.urlEndPoint).pipe(
      map( response => response as Rol[])
    );
  }
}
